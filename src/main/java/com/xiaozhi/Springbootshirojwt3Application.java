package com.xiaozhi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootshirojwt3Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootshirojwt3Application.class, args);
    }

}
