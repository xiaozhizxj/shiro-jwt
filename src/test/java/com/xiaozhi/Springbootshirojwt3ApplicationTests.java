package com.xiaozhi;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Springbootshirojwt3ApplicationTests {

    @Test
    void contextLoads() {
        String password = new SimpleHash("SHA-1", "123456", "python", 16).toString();
        System.out.println(password);
    }

}
